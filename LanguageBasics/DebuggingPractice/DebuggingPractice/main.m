//
//  main.m
//  DebuggingPractice
//
//  Created by Nick Oman on 1/14/17.
//  Copyright © 2017 Oxford Comma. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
    }
    return 0;
}
