//
//  main.m
//  ifElseStatements
//
//  Created by Nick Oman on 1/8/17.
//  Copyright © 2017 Oxford Comma. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
    }
 
    
    
    
    // reg price $10
    // senior price $5
    // Matinee Price $4
    
    
    bool isMatinee = TRUE;
 
    float regPrice = 10;
    float seniorPrice = 5;
    float matineePrice = 4;
    
    
    int minAge = 60;
    
    int custAge = 55;
    
    float custPrice;
    
    
    if (isMatinee) {
    
        custPrice = matineePrice;
        
    } else if (custAge >=  minAge){
    
        custPrice = seniorPrice;
   
    } else {
        
        custPrice = regPrice;
    
    }
    

    // test question
    
    
    int 	customerAge;
    float 	ticketPrice;
    
    customerAge = 11;
    
    if(customerAge < 13){
        ticketPrice = 5.00;
    }
    
    else if (customerAge >= 65){
        ticketPrice = 6.00;
    }
    
    else{
        ticketPrice = 10.00;
    }
    
    

    
    
    
    return 0;
    
}
