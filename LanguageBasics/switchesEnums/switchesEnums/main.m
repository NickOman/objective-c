//
//  main.m
//  switchesEnums
//
//  Created by Nick Oman on 1/8/17.
//  Copyright © 2017 Oxford Comma. All rights reserved.
//



enum popcornSizes{

     KidsPopcorn = 1,
     smallPopcorn = 2,
     mediumPopcorn = 3,
     largePopcorn = 4,
     tubPopcorn = 5
    
};


#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
    }
    
    
    
    // Popcorn sizes and prices
    // Kids - 1.50
    // Small - 3.00
    // Medium - 4.25
    // Large - 5.25
    // Tub - 6.00
    
    float popcornPrice;
    int popcornSize = mediumPopcorn;
    
    switch (popcornSize) {
        
        case KidsPopcorn:
            popcornPrice = 1.5;
            break;
       
            
        case smallPopcorn:
            popcornPrice = 3;
            break;
            
        case mediumPopcorn:
            popcornPrice = 4.25;
            break;
            
        case largePopcorn:
            popcornPrice = 5.25;
            break;
            
            
        case tubPopcorn:
            popcornPrice = 6.00;
            break;
            
        default:
            
            NSLog(@"No valid size entered");
            
            break;
    }
    
    
    
    
    
    
    return 0;
}
